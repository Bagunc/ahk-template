import { src, dest, series } from 'gulp'

import clean from 'gulp-clean'
import { existsSync, mkdirSync } from 'fs'

const build = next => {

  try {
    existsSync('src/assets/img') &&
      !existsSync('public/assets/img') &&
        mkdirSync('public/assets/img')
  } catch(error) {
    console.log("Images build error");
    console.error(error);
  }

  next()
}

export const cleaning = next => {
  try {
    existsSync('public/assets/img') && 
      src('public/assets/img', { 
        read: false,
        allowEmpty: true
      })
      .pipe(clean({ 
        force: true 
      }))
  } catch(error) {
    console.log("Images cleaning error")
    console.error(error)
  }

  next();
}

export const execute = browser => next => {

  src([
    'src/assets/img/**/*'
  ]).on('error', error => console.log(error))
  .pipe(dest('public/assets/img')).on('error', error => console.log(error))
  .pipe(browser.stream())

  next()
}

export default browser => series(
  execute(browser)
)