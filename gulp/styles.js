import { src, dest, series } from 'gulp'

import sass from 'gulp-sass'
import clean from 'gulp-clean'
import rename from 'gulp-rename'
import sourcemaps from 'gulp-sourcemaps'
import { existsSync, mkdirSync } from 'fs'
import autoprefixer from 'gulp-autoprefixer'

import { isProd } from './utils'

const build = next => {

  try {
    existsSync('src/assets/sass') &&
      !existsSync('public/assets/css') &&
        mkdirSync('public/assets/css')
  } catch(error) {
    console.log("Styles build error")
    console.error(error)
  }

  next()
}

export const cleaning = naxt => {

  try {
    existsSync('public/assets/css') &&
      src('public/assets/css', { 
        read: false,
        allowEmpty: true
      })
      .pipe(clean({ 
        force: true 
      }))
  } catch (error) {
    console.log("Styles cleaning error")
    console.error(error)
  }

  naxt()
}

export const execute = browser => next => {

  try {
    src('src/assets/sass/**/*')
    .pipe(sourcemaps.init({
      loadMaps: true,
    }))
    .pipe(sass({ 
      outputStyle: isProd ? 'compressed' : 'expanded',
    }).on('error', sass.logError))
    .pipe(autoprefixer({
      cascade: false,
      overrideBrowserslist:  ['last 4 versions'],
    }))
    .pipe(rename({
      extname: isProd ? '.min.css' : '.css',
    }))
    .pipe(sourcemaps.write('./'))
    .pipe(dest('public/assets/css'))
    .pipe(browser.stream())
  } catch (err) {
    console.log(err)
  }

  next()
}

export default (browser) => series(
  execute(browser)
)