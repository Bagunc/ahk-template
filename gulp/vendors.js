import { src, dest, series } from 'gulp'

import clean from 'gulp-clean'
import { existsSync, mkdirSync } from 'fs'

const build = next => {

  try {
    existsSync('src/assets/vendors') &&
      !existsSync('public/assets/vendors') &&
        mkdirSync('public/assets/vendors')
  } catch(error) {
    console.log("Vendors build error")
    console.error(error)
  }
    
  next()
}

export const cleaning = naxt => {

  try {
    existsSync('public/assets/vendors') &&
      src('public/assets/vendors', { read: false })
      .pipe(clean({ force: true }))
  } catch(error) {
    console.log("Vendors cleaning error")
    console.error(error)
  }

  naxt()
}

export const execute = browser => next => {
  
  src('src/assets/vendors/**/*')
  .pipe(dest('public/assets/vendors'))
  .pipe(browser.stream())

  next()
}

export default browser => series(
  execute(browser)
)