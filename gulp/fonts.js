import { src, dest, series } from 'gulp'

import clean from 'gulp-clean'
import { existsSync, mkdirSync } from 'fs'

const build = next => {

  try {
    if (existsSync('src/assets/fonts') && !existsSync('public/assets/fonts'))
      mkdirSync('public/assets/fonts')
  } catch (error) {
    console.log("Fonts build error")
    console.error(error)    
  }

  next()
}

const cleaning = next => {

  try {
    existsSync('public/assets/fonts') && 
      src('public/assets/fonts', { read: false })
      .pipe(clean({ force: true }))
  } catch(error) {
    console.log("Fonts cleaning error")
    console.error(error)
  }

  next()
}

const execute = browser => next => {

  src('src/assets/fonts/**/*')
  .pipe(dest('public/assets/fonts'))
  .pipe(browser.stream())

  next()
}

export default browser => series(
  execute(browser)
)