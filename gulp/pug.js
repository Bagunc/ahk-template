import { src, dest, series } from 'gulp'

import pug from 'gulp-pug'
import { existsSync, mkdirSync } from 'fs'

const build = next => {

  try {
    existsSync('src') &&
      !existsSync('public') &&
        mkdirSync('public')

    !existsSync('public/assets') &&
      mkdirSync('public/assets')

    existsSync('src/favicon.ico') &&
      src('src/favicon.ico')
        .pipe(dest('public'))
  } catch(error) {
    console.log("PUG build error")
    console.error(error)
  }

  next()
}

const execute = browser => next => {

  src([
    'src/**/*.pug', 
    '!src/**/_*.pug', 
    '!src/layouts/**/*.pug',
    '!src/includes/**/*.pug',
  ])
  .pipe(pug({
    pretty: true
  }).on('error', error => {
    console.log(error)
    next(error)
  }))
  .pipe(dest('public'))
  .pipe(browser.stream())

  next()
}

export default browser => series(
  execute(browser)
)