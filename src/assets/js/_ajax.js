/* global jQuery, YT, lazyLoad */

jQuery($ => {

  $(document).ready(() => {

    if ($('.article--gallery').length)
      magnificPopup('.article--gallery')
    
    window.siteLoading(false)
  })


  $(document).on('click', '.discussion__vote', function(e) {
    e.preventDefault();

    window.siteLoading(true)

    const $this = $(this);
    
    const vote = $this.data('vote')
    const action = "discussion__vote"
    const nonce = ajax__localize.nonce
    const discussion = parseInt($this.data('id'))

    $.post(ajax__localize.ajax__url, {
      vote,
      nonce,
      action,
      discussion,
    }, response => {
      response = typeof response === 'string' ? JSON.parse(response) : response;

      if (response.status)
        if (response.status == 200)
          $.get(window.location, {}, response => {
            const $response = $($.parseHTML(response))
            const $output = $($response.find('.section--contact-voting'))

            $('.section--contact-voting').replaceWith($output)

            window.siteLoading(false)
          })
        else
          console.error(`Error [${response.status}]: ${response.message}`);
      else
        console.error(ajax__localize.text.ajax__error)
    })
  })

  $(document).on('click', '.gallery__item--ajax', galleryModal)

  $(document).on('click', '.button--ajax', event => {
    event.preventDefault()

    const $this = $(event.currentTarget)

    const id = $this.attr("id")
    const url = $this.attr("href")
    const node = $this.data("node")
    const action = $this.data("action")

    const $node = $(node)

    $.get(url, response => {

      const $response = $($.parseHTML(response))

      const $output = $($response.find(node))
      const $self = $($response).find(`#${id}`)

      const method = action ? action : "append"

      if ($output.length && $node[method])
        $node[method]($output.html())

      if (!$self.length)
        $this.remove()
      else
        $this.replaceWith($self)

      if (url)
        window.history.pushState(url, document.title, url)

      lazyLoad()
    })

  })

  function galleryModal(event) {
    event.preventDefault()

    $('.modal').modal("hide")

    const $gitem = $(event.currentTarget)
    const $this = $(event.currentTarget).is("a") ? $(event.currentTarget) : $(event.currentTarget).find("a")

    const type = (() => {
      if ($gitem.hasClass("gitem--photo"))
        return "photo"

      if ($gitem.hasClass("gitem--video"))
        return "video"

      if ($gitem.hasClass("gpreview--video"))
        return "video"

      if ($gitem.hasClass("gpreview"))
        return "photo"
    })()

    if (type === "photo")
      siteLoading()

    const ID = (() => {

      if ($gitem.data("id"))
        return $gitem.data("id")

      if ($gitem.attr("id"))
        return `modal-${$gitem.data("id")}`

      return `modal-${Math.random().toString(36).substr(2)}`
    })()

    const link = $this.attr("href")

    try {
      switch (type) {
        case 'photo':
          const modal = document.querySelector('#modal-template')
          const $modal = modal.content.cloneNode(true)

          $modal.querySelector("#simple-modal").id = ID

          if ($modal)
            $.get(link, response => {
              siteLoading(false)

              const $response = $($.parseHTML(response))
              const $output = $($response.find('.article--gallery'))
              const $nav = $output.find('nav.gallery__siblings')

              $output.find('.gallery__thumbnail').append($nav)

              $($modal).find(".modal-body").append($output)
              $($modal).find(".modal-content").append(`<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <svg><use xlink:href="${ajax__localize.icons}#close"></use></svg>
                                                      </button>`)

              window.addEventListener("keyup", galleryKeyNavigation)
              $("body").append($modal.cloneNode(true))
              $(`#${ID}`).modal("show").on('hidden.bs.modal', function () {
                $(`#${ID}`).remove()
                window.removeEventListener("keyup", galleryKeyNavigation)
              })

              lazyLoad()
              magnificPopup('.article--gallery')
            })
          break
        case "video":

          if ($gitem.hasClass("gallery__item--big")) return

          const index = $gitem.index() + 1
          const videoId = $this.data("v")
          const isRowLast = index % 3 === 0

          const $wrapper = $gitem.parents(".gallery--video")
          $wrapper.find(".gitem__thumbnail iframe").remove()

          if (!videoId)
            window.location.href = link

          if (isRowLast && screen.width >= 1200)
            $gitem.insertBefore($gitem.prev(".gitem"))

          $wrapper.find(".gallery__item--big").removeClass("gallery__item--big")
          $gitem.addClass("gallery__item--big")

          const $thumbnail = $gitem.find(".gitem__thumbnail")

          $("html, body").animate({
            scrollTop: (() => {
              let top = $gitem.position().top

              const menu = $(".wrapper__menu").height()
              const bar = parseFloat($("html").css("margin-top"))

              top -= bar

              if (screen.width <= 991)
                top -= menu

              return top
            })()
          }, 500)

          if (YT) {
            $thumbnail.append('<div id="youtube-player"></div>')

            new YT.Player('youtube-player', {
              videoId,
              autoplay: 1,
              width: '100%',
              height: '100%',
              events: {
                onReady: event => {
                  event.target.playVideo()

                  if (screen.width <= 575) {
                    const iframe = document.querySelector("#youtube-player")
                    const requestFullScreen = (
                      iframe.requestFullScreen ||
                      iframe.mozRequestFullScreen ||
                      iframe.webkitRequestFullScreen
                    )

                    if (requestFullScreen)
                      requestFullScreen.bind(iframe)();
                  }
                }
              }
            });
          } else {
            $thumbnail.append(`<iframe width="100%"
                                        height="100%"
                                        id="ytplayer"
                                        frameborder="0"
                                        type="text/html"
                                        src="https://youtube.com/embed/${videoId}?autoplay=1&origin=${window.location.origin}"
                                      ></iframe>`)
          }
          break
      }
    } catch (e) {
      window.location.href = link
    }
  }

  function galleryKeyNavigation(event) {
    event.preventDefault()

    if (!$('.modal--gallery').length) return

    const keyCode = event.which ? event.which : event.keyCode

    switch(keyCode) {
      case 37:
        const $prev = $('.modal--gallery .article--gallery .gallery__siblings .siblings__item--prev');
        if ($prev.length)
          $prev.click()
        break
      case 39:
        const $next = $('.modal--gallery .article--gallery .gallery__siblings .siblings__item--next');
        if ($next.length)
          $next.click()
        break
      default:
        break
    }

  }

  function magnificPopup(selector = null, config = {}) {

    if (!selector) return

    config = Object.assign({
      type: 'image',
      mainClass: 'mfp-with-zoom',
      delegate: 'a.magnific-popup',
      gallery:{
        enabled:true
      },
      zoom: {
        enabled: true,
        duration: 300,
        easing: 'ease-in-out',
      },
    }, config)

    const $images = $(selector).find("img")
    $images.each((index, item) => {
      const $img = $(item)
      const $link = $($img.parents("a"))

      if (!$link.length)
        $img.wrap(`<a class="magnific-popup" href="${$(item).data('src') || $(item).attr("src")}"></a>`)
    })

    $(selector).magnificPopup(config)

  }

})

window.siteLoading = (show = true) => {

  const template = document.querySelector("#loading-template")
  const node = "content" in template ? template.content.cloneNode(true) : null;

  if (!node) return

  const $node = jQuery(node)

  const $body = jQuery("body");
  const $defs = jQuery(".template-defs")

  if (show) {
    $body.append(node)
    $defs.modal("show")
    $body.addClass("body--loading")
  } else {
    $defs.modal("hide")
    jQuery(".site-loading").remove()
    $body.removeClass("body--loading")
  }
}