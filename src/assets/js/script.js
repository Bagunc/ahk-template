'use strict'

import './_ajax.js'

window.lazyLoad = () => {
  const lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));

  if ("IntersectionObserver" in window) {
    let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          let lazyImage = entry.target;
          lazyImage.src = lazyImage.dataset.src;
          lazyImage.srcset = lazyImage.dataset.srcset;
          lazyImage.classList.remove("lazy");
          lazyImage.classList.add("lazy--loaded");
          lazyImageObserver.unobserve(lazyImage);
        }
      });
    });

    lazyImages.forEach(function(lazyImage) {
      lazyImageObserver.observe(lazyImage);
    });
  }
}

const initPrefectScrollBar = () =>
  document.querySelectorAll('.ps').forEach(node => new PerfectScrollbar(node))

jQuery($ => {

  $('#main-nav a[href="#"]').click(e => e.preventDefault())

  $('#main-nav a .svg--next').click(e => {
    e.preventDefault()

    const $this = $(e.target)
    const $item = $this.parents('.menu-item')

    $item.toggleClass('menu-item--open')
  })

  $(document).on('click', '.button--more', e => $(e.currentTarget).addClass('button--loading'))

  $(document).ready(() => {
    
    window.lazyLoad()
    initPrefectScrollBar()

    const structures = $('.structure:not(.structure--ready)')
    const postThumbnails = $('.post.post--thumbnail:not(.post--ready)')


    $.each(structures, index => {

      const $this = $(structures.get(index))
      const $row = $this.find('.row--item')
      $this.css('height', $this.height())

      $row.css('max-height', $row.height())

      $this.addClass('structure--ready')
    })

    $.each(postThumbnails, index => {

      const $this = $(postThumbnails.get(index)) 
      const $hide = $this.find('.post__hidden')

      if ($hide.length)
        $hide.css('height', $hide.height())

      $this.addClass('post--ready')
    })

    if ($('.carousel--slick').slick)
      $('.carousel--slick').slick({
        arrows: true,
        autoplay: true,
        infinite: true,
        autoplaySpeed: 5000,
        
        prevArrow: `<button type="button" class="carousel__arrow carousel__arrow--prev d-none d-sm-inline">
                      <svg>
                        <use xlink:href="${ajax__localize.icons}#next"></use>
                      </svg>
                    </button>`,
        nextArrow: `<button type="button" class="carousel__arrow carousel__arrow--next d-none d-sm-inline">
                      <svg>
                        <use xlink:href="${ajax__localize.icons}#next"></use>
                      </svg>
                    </button>`,
      })
    
    $(document).on("click", "#menu-toggle", e => {
      e.preventDefault()

      $('body').addClass('modal-open')
      $('.menu').addClass('open')
    })

    $(document).on("click", ".menu .overlay", event => {
      event.preventDefault()

      $('body').removeClass('modal-open')
      $('.menu').removeClass('open')
    })

  })

})